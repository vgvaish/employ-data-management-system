import pymysql

from flask import Flask, jsonify, request
from flask_cors import CORS
import pymysql

app = Flask(__name__)
cors = CORS(app)



@app.route('/employee', methods=['GET'])
def get_users():
    # To connect MySQL databasehttp://127.0.0.1:5000/
    conn = pymysql.connect(host='localhost', user='root', password = "", db='employee_db')

    cur = conn.cursor()
    

    cur.execute(f"select * from employee order by created_at desc");

    output = cur.fetchall()

    print(type(output)); #this will print tuple

    for rec in output:
        print(rec);

    conn.close()

    return jsonify(output);

@app.route('/employee/id', methods=['GET'])
def read():
    # To connect MySQL database
    conn = pymysql.connect(host='localhost', user='root', password = "", db='employee_db')
    cur = conn.cursor()
    id = int(request.args.get('id'))
    cur.execute(f"select * from employee WHERE id = {id}");

    output = cur.fetchall()

    print(type(output)); #this will print tuple

    for rec in output:
        print(rec);

    # To close the connection
    conn.close()

    return jsonify(output);

@app.route('/employee', methods=['DELETE'])
def deleteRecord():
    #return f"{id} record deleted"

    conn = pymysql.connect(host='localhost', user='root', password = "", db='employee_db')
    cur = conn.cursor()
    id = int(request.args.get('id'));
    
    #return f"{conn}"
    
    query = f"delete from employee where id = {id}";
    #return query
    
    res = cur.execute(query);
    conn.commit();
    print(cur.rowcount, "record(s) deleted")

    return "Record deleted sussesfully"
   

@app.route('/employee', methods=['POST'])
def insertRecord():
    conn = pymysql.connect(host='localhost', user='root', password = "", db='employee_db')
    
    raw_json = request.get_json();
    name = raw_json['name'];
    contact_number = raw_json['contact_number'];
    address = raw_json['address'];
    salary = raw_json['salary'];
    employee_id =raw_json['employee_id'];
    role = raw_json['role'];
    
    sql = f"INSERT INTO `employee` (`id`, `name`, `contact_number`, `address`, `salary`, `employee_id`, `role`, `created_at`, `updated_at`) VALUES (NULL, '{name}', '{contact_number}', '{address}', '{salary}', '{employee_id}', '{role}', NOW(), NOW())";
	
    cur = conn.cursor();
    cur.execute(sql);
    conn.commit()

    return {"result":"Record inserted Succesfully"};

@app.route('/employee', methods=['PUT'])
def updateRecord():
    conn = pymysql.connect(host='localhost', user='root', password = "", db='employee_db')

    raw_json = request.get_json();

    id = raw_json['id'];
    raw_json = request.get_json();
    name = raw_json['name'];
    contact_number = raw_json['contact_number'];
    address = raw_json['address'];
    salary = raw_json['salary'];
    employee_id =raw_json['employee_id'];
    role = raw_json['role'];

    
    sql = (f"UPDATE employee_db.employee SET name = '{name}', contact_number = '{contact_number}', address = '{address}', salary = '{salary}', employee_id = '{employee_id}', role = '{role}' WHERE id = '{id}'");

    cur = conn.cursor();
    cur.execute(sql);
    conn.commit()

    return {"result":"Record updated Succesfully"};

if __name__ == "__main__":
   app.run(debug=True);

