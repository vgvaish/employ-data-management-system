-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2021 at 11:31 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `employee_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(6) UNSIGNED NOT NULL,
  `name` varchar(30) NOT NULL,
  `contact_number` varchar(13) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `salary` int(10) DEFAULT NULL,
  `employee_id` int(10) DEFAULT NULL,
  `role` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `name`, `contact_number`, `address`, `salary`, `employee_id`, `role`, `created_at`, `updated_at`) VALUES
(1, 'Nehav Nikk', '9833910534', 'mumbai', 30000, 98821, 'reconciliation of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(2, 'Mina Tiwari', '9833910535', 'thane', 32000, 98823, 'mangagement of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(3, 'Pankaj Nimange', '9833910536', 'bhopal', 40000, 98824, 'valuing of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(4, 'Minal  Kumar', '9833910537', 'meerut', 45000, 98825, 'revaluation of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(5, 'Pooja Kapoor', '9833910538', 'delhi', 50000, 98826, 'reconciliation of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(6, 'Namita Tanna', '9833910539', 'surat', 52000, 98820, 'reconciliation of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(7, 'Sneha Roy', '9833910510', 'baroda', 55000, 98827, 'reconciliation of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(8, 'Anjali  Patil', '9833910511', 'ahmedabad', 60000, 98828, 'reconciliation of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(9, 'Harsha Das', '9833910512', 'mumbai', 20000, 98829, 'reconciliation of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(10, 'Varun Naik', '9833910512', 'mehsana', 56000, 98831, 'reconciliation of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(11, 'Preeti  Singh', '9833910513', 'noida', 87000, 98832, 'reconciliation of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(12, 'Mohan Roy', '9833910525', 'bangalore', 22000, 98833, 'reconciliation of transactions', '2021-05-27 00:00:00', '2021-05-27 00:00:00'),
(13, 'roshan jadhav', '9123456432', 'pune', 40000, 12020, 'management of transaction', '2021-09-23 04:23:10', '2021-09-29 08:10:19'),
(14, 'pallavi mungade', '198766785', 'nagpur', 45000, 12021, 'manager ', '2021-09-23 05:23:18', '2021-09-25 06:11:53'),
(15, 'nakul meheta', '9156743280', 'nashik', 32000, 12022, 'reconciliation of transaction ', '2021-09-22 06:09:32', '2021-09-30 10:20:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
